
public class Variable {

	public static void main(String[] args) {
		int	a = 100;
		float b = 20.22f;
		double c = 345.3;
		long d = 3949048;
		short e = 3394;
		char YesOrNo= 'y';
		boolean TrueOrFalse = true ;
		
		System.out.println("Integer:" + a);
		System.out.println("Float:" + b);
		System.out.println("Double:" +c );
		System.out.println("Long:" + d);
		System.out.println("Short:" +e);
		System.out.println("Choice="+ YesOrNo);
		System.out.println("BooleanValue=" + TrueOrFalse);
	}
	
}
	
